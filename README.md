# Motion Sonif: software for wearable devices
Contacts: 
- danilo.spada@harmonio.org  // project administration and infos
- masciadri.andrea@gmail.com // software and installation issues

# Assembling the devices
- Connect the battery (Lithium Ion Polymer Battery - 3.7v) to the LiPo Charger Circuit (e.g. Adafruit Micro-Lipo Charger for LiPo/LiIon Batt w/MicroUSB Jack): B+ (red) and B- (black) pins
- Connect the LiPo Charger Circuit to the Arduino: Vin (red) and GND (black) pins
- [optional] place a switch button on the red wire to turn on/off the device
- connect the Force sensor to the AnalogPin0 and GND creating a voltage divider with a 1kohm resistor

# Installation notes
 1. Arduino Sketch Download
 Download the file c4c_wearable_pressure.ino from the main branch of this repository

 2. Upload the Sketch on the Arduino Board
 - Register and login to the Arduino Online editor at https://create.arduino.cc/editor
 - Click on "New Sketch" and paste the code from c4c_wearable_pressure.ino
 - Connect the Arduino Board to the PC using the USB cable
 - Select the board "Arduino Nano 33 IOT" from the top bar and then press the button "Upload and save"
